#ifndef FONCTION_MATH
#define FONCTION_MATH

double carre(double n);

double puissance(double n, double p);

double racine(double n);

#endif
