#ifndef TP3_HPP
#define TP3_HPP

double aireRectangle(double longueur, double largeur);

double aireCercle(double diametre);

double saisieDouble(double min, double max);

void aires();

int fibonacci_boucle(int n);

int fibonacci_recursive(int n);

#endif
